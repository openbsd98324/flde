

# COPY ROOTFS 

wget -c --no-check-certificate   "https://gitlab.com/openbsd98324/devuan-rescue/-/raw/main/ascii-desktop/amd64/DEVUAN-RESCUE-X11-DESKTOP.tar.gz" 
  

You will have the file : DEVUAN-RESCUE-X11-DESKTOP.tar.gz

98e5563b9c0650b687928c07824fa7ef  DEVUAN-RESCUE-X11-DESKTOP.tar.gz



mkfs.ext3 /dev/sda4

mkdir /media/sda4 

mount /dev/sda4 /media/sda4 

cd /media/sda4

tar xvpfz  DEVUAN-RESCUE-X11-DESKTOP.tar.gz


# GRUB 

menuentry 'Linux Devuan X11, Devuan on sda4' --class devuan --class gnu-linux --class gnu { 
        insmod gzio
        insmod part_msdos
        insmod ext2
        set root='hd0,msdos4'
        linux   /boot/vmlinuz-4.9.0-11-amd64 root=/dev/sda4 rw 
        initrd  /boot/initrd.img-4.9.0-11-amd64
}


This will give a desktop (minimal installation, with kicker) with FLDE running on devuan ascii.


KEYS: 

win key + w : close app

win key + r : flcommand 

win key + e : flm, the file explorer

win key + c : xterm 


