
/// MOONCAL.C    
/// Compile with :  cc -lm mooncal.c -o mooncal ; ./mooncal   
/// Compiled on OPENBSD.

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <time.h>

double moon_phase(int year,int month,int day, double hour);

/* source: http://www.voidware.com/phase.c */
/* standalone moon phase calculation */
#include <stdio.h>
#include <math.h>
#define PI 3.1415926535897932384626433832795
#define RAD (PI/180.0)
#define SMALL_FLOAT (1e-12)

typedef struct {
	int year,month,day;
	double hour;
} TimePlace;

void JulianToDate(TimePlace* now, double jd)
{
	long jdi, b;
	long c,d,e,g,g1;

	jd += 0.5;
	jdi = jd;
	if (jdi > 2299160) {
		long a = (jdi - 1867216.25)/36524.25;
		b = jdi + 1 + a - a/4;
	}
	else b = jdi;

	c = b + 1524;
	d = (c - 122.1)/365.25;
	e = 365.25 * d;
	g = (c - e)/30.6001;
	g1 = 30.6001 * g;
	now->day = c - e - g1;
	now->hour = (jd - jdi) * 24.0;
	if (g <= 13) now->month = g - 1;
	else now->month = g - 13;
	if (now->month > 2) now->year = d - 4716;
	else now->year = d - 4715;
}

double
Julian(int year,int month,double day)
{
	/*
      Returns the number of julian days for the specified day.
	*/

	int a,b,c,e;
	if (month < 3) {
		year--;
		month += 12;
	}
	if (year > 1582 || (year == 1582 && month>10) ||
	    (year == 1582 && month==10 && day > 15)) {
		a=year/100;
		b=2-a+a/4;
	}
	c = 365.25*year;
	e = 30.6001*(month+1);
	return b+c+e+day+1720994.5;
}

double sun_position(double j)
{
	double n,x,e,l,dl,v;
	int i;

	n=360/365.2422*j;
	i=n/360;
	n=n-i*360.0;
	x=n-3.762863;
	if (x<0) x += 360;
	x *= RAD;
	e=x;
	do {
		dl=e-.016718*sin(e)-x;
		e=e-dl/(1-.016718*cos(e));
	} while (fabs(dl)>=SMALL_FLOAT);
	v=360/PI*atan(1.01686011182*tan(e/2));
	l=v+282.596403;
	i=l/360;
	l=l-i*360.0;
	return l;
}

double moon_position(double j, double ls)
{

	double ms,l,mm,n,ev,sms,ae,ec;
	int i;

	/* ls = sun_position(j) */
	ms = 0.985647332099*j - 3.762863;
	if (ms < 0) ms += 360.0;
	l = 13.176396*j + 64.975464;
	i = l/360;
	l = l - i*360.0;
	if (l < 0) l += 360.0;
	mm = l-0.1114041*j-349.383063;
	i = mm/360;
	mm -= i*360.0;
	n = 151.950429 - 0.0529539*j;
	i = n/360;
	n -= i*360.0;
	ev = 1.2739*sin((2*(l-ls)-mm)*RAD);
	sms = sin(ms*RAD);
	ae = 0.1858*sms;
	mm += ev-ae- 0.37*sms;
	ec = 6.2886*sin(mm*RAD);
	l += ev+ec-ae+ 0.214*sin(2*mm*RAD);
	l= 0.6583*sin(2*(l-ls)*RAD)+l;
	return l;
}

double moon_phase(int year,int month,int day, double hour/*, int* ip*/)
{
	/*
      Calculates more accurately than Moon_phase , the phase of the moon at
      the given epoch.
      returns the moon phase as a real number (0-1)
	*/

	double j= Julian(year,month,(double)day+hour/24.0)-2444238.5;
	double ls = sun_position(j);
	double lm = moon_position(j, ls);

	double t = lm - ls;
	if (t < 0) t += 360;
	return fmod((t /*+ 22.5*/)/45, 8); //(int)... & 7
	//return (1.0 - cos((lm - ls)*RAD))/2;
}

void print_center(char *str, int width){
	int len = strlen(str);
	int before = (width-len)/2;
	printf("%*s\n", before+len, str);
}

// re-calculate date to fill in day of week etc.
void fixtime(struct tm **ts){
	time_t t = mktime(*ts);
	*ts = localtime(&t);
}

int main(int argc, char **argv){
	// get first day of current month
	time_t current_time;
	time(&current_time);
	struct tm *date = localtime(&current_time);
	date->tm_mday = 1;
	// adjust month/year if needed
	if (argc >= 2)
		date->tm_mon = atoi(argv[1])-1;
	if (argc >= 3)
		date->tm_year = atoi(argv[2])-1900;

	fixtime(&date);
	
	int month = date->tm_mon;
	int year = date->tm_year+1900;
	int firstday = date->tm_wday;

	// print header
	char title[80];
	strftime(title, sizeof(title), "%B %Y", date);
	const char row[] = "+-----+-----+-----+-----+-----+-----+-----+";
	print_center(title, sizeof(row));
	puts("  Sun   Mon   Tue   Wed   Thu   Fri   Sat");
	puts(row);

	// get next month
	if (++(date->tm_mon) == 12) {
		date->tm_mon = 0;
		date->tm_year++;
	}
	fixtime(&date);
	// Calculate number days in current month
	int days_in_month = (date->tm_wday-firstday+7)%7 +28;

	// Moon phases
	char moon[32] = {0};
	int day;
	double ph, oph;
	for (day=0; day<=days_in_month+1; day++) {
		oph = ph;

		ph = moon_phase(year, month+1, day, 12.00);

		//time depends on location + moon location. 12:00 average is ok I guess
		if (day) {
			// put icons on days closest to each phase
			if (ph < oph)
				moon[day - (8-oph < ph)] = '.'; //new
			else if (oph < 4 && ph >= 4)
				moon[day - (4-oph < ph-4)] = 'O'; //full
			else if (oph < 2 && ph >= 2)
				moon[day - (2-oph < ph-2)] = ')'; //first quarter
			else if (oph < 6 && ph >= 6)
				moon[day - (6-oph < ph-6)] = '('; //last quarter
		}
	}

	// print main calendar
	int day_of_week = 0;
	int half = 0;
	day = -firstday+1;
	while (1) {
		if (day<=0 || day>days_in_month) { //empty cell
			printf("!     ");
		} else {
			if (!half) { // top half
				printf("!%-5d", day);
				//printf("|%-5d", day);
			} else { // bottom half
				if (moon[day])
					printf("!    %c", moon[day]);
					//printf("|....%c", moon[day]);
				else
					printf("!     ");
					//printf("|.....");
			}
		}
		day++;
		// end of row
		if (++day_of_week == 7) {
			day_of_week = 0;
			puts("!");
			if (!half) { // top half
				half = 1;
				day -= 7;
			} else { // bottom half
				puts(row);
				if (day>days_in_month)
					break;
				half = 0;
			}
		}
	}
	return 0;
}






