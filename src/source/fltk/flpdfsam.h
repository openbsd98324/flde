// generated by Fast Light User Interface Designer (fluid) version 1.0304

#ifndef flpdfsam_h
#define flpdfsam_h
#include <FL/Fl.H>
#include <FL/Fl_Double_Window.H>
extern Fl_Double_Window *win1;
#include <FL/Fl_Box.H>
#include <FL/Fl_Tabs.H>
#include <FL/Fl_Group.H>
#include <FL/Fl_Browser.H>
extern Fl_Browser *plot_datafile_multicurve_browser1;
#include <FL/Fl_Button.H>
#include <FL/Fl_Input.H>
extern Fl_Input *input_str_filter;
#include <FL/Fl_File_Browser.H>
extern Fl_File_Browser *panel2_list;
#include <FL/Fl_Output.H>
extern Fl_Output *output_curdir;
extern Fl_Input *panel2_inputbox_usersel_filename;
extern Fl_Output *win1_statusbar_content;
Fl_Double_Window* make_window();
int main( int argc, char *argv[]);
#endif
