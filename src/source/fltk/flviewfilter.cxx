// generated by Fast Light User Interface Designer (fluid) version 1.0304

#include "flviewfilter.h"
#include <stdio.h>
#include <stdlib.h>
#include "ccode.h"
static char user_arg1[PATH_MAX]; 
static int flfontsize = 14; 

void loadpanel_mark( const char *pattern ) {
  printf("Load Panel!\n");
  
  
    
    int fetchi;
    FILE *fp5;
    FILE *fp6;
    char fetchline[PATH_MAX];
    char fetchlinetmp[PATH_MAX];
    char foostr[PATH_MAX];
  
      fbrow->clear( );    
      
      fp6 = fopen( pattern , "rb");
      while( !feof(fp6) ) 
      {
            fgets(fetchlinetmp, PATH_MAX, fp6); 
            strncpy( fetchline, "" , PATH_MAX );
            for( fetchi = 0 ; ( fetchi <= strlen( fetchlinetmp ) ); fetchi++ )
            
              if ( fetchlinetmp[ fetchi ] != '\n' )
                   fetchline[fetchi]=fetchlinetmp[fetchi];
                   
                  if ( !feof( fp6 ) ) 
                  {
                      if ( strstr( fetchline, inputbox_filter_str->value() ) != 0 )
                      {
                         snprintf( foostr, sizeof( foostr ) , "@b%s", fetchline  );  
                         fbrow->add( foostr ); 
                      }    
                      else
                          fbrow->add( fetchline );    
                  }
  
       }
       fclose( fp6 );
       
     //  inputbox_filter_str
}

void loadpanel_filter( const char *pattern ) {
  printf("Load Panel!\n");
  
  
    
    int fetchi;
    FILE *fp5;
    FILE *fp6;
    char fetchline[PATH_MAX];
    char fetchlinetmp[PATH_MAX];
    char foostr[PATH_MAX];
  
      fbrow->clear( );    
      
      fp6 = fopen( pattern , "rb");
      while( !feof(fp6) ) 
      {
            fgets(fetchlinetmp, PATH_MAX, fp6); 
            strncpy( fetchline, "" , PATH_MAX );
            for( fetchi = 0 ; ( fetchi <= strlen( fetchlinetmp ) ); fetchi++ )
            
              if ( fetchlinetmp[ fetchi ] != '\n' )
                   fetchline[fetchi]=fetchlinetmp[fetchi];
                   
                  if ( !feof( fp6 ) ) 
                  {
                      if ( strstr( fetchline, inputbox_filter_str->value() ) != 0 )
                      {
                         snprintf( foostr, sizeof( foostr ) , "@b%s", fetchline  );  
                         fbrow->add( foostr ); 
                      }    
                      // else
                         // fbrow->add( fetchline );    
                  }
  
       }
       fclose( fp6 );
       
     //  inputbox_filter_str
}

void loadpanel( const char *pattern ) {
  printf("Load Panel!\n");
  
  
    
    int fetchi;
    FILE *fp5;
    FILE *fp6;
    char fetchline[PATH_MAX];
    char fetchlinetmp[PATH_MAX];
  
      fbrow->clear( );    
      
      fp6 = fopen( pattern , "rb");
      while( !feof(fp6) ) 
      {
            fgets(fetchlinetmp, PATH_MAX, fp6); 
            strncpy( fetchline, "" , PATH_MAX );
            for( fetchi = 0 ; ( fetchi <= strlen( fetchlinetmp ) ); fetchi++ )
              if ( fetchlinetmp[ fetchi ] != '\n' )
                   fetchline[fetchi]=fetchlinetmp[fetchi];
                   
                  if ( !feof( fp6 ) ) 
                  {
                   fbrow->add( fetchline );    
                  }
  
       }
       fclose( fp6 );
}

Fl_Double_Window *win1=(Fl_Double_Window *)0;

static void cb_Re(Fl_Button*, void*) {
  loadpanel( user_arg1 );
}

Fl_Browser *fbrow=(Fl_Browser *)0;

static void cb_Quit(Fl_Button*, void*) {
  exit( 0 );
}

static void cb_Font(Fl_Button*, void*) {
  flfontsize++;
   fbrow->labelsize( flfontsize  );
   fbrow->textsize( flfontsize  );


//loadpanel( user_arg1 );
}

static void cb_Font1(Fl_Button*, void*) {
  flfontsize--;

fbrow->labelsize( flfontsize  );
   fbrow->textsize( flfontsize  );


//loadpanel( user_arg1 );
}

Fl_Input *inputbox_filter_str=(Fl_Input *)0;

static void cb_Page(Fl_Button*, void*) {
  fbrow->value(  fbrow->value( ) - 10 );
}

static void cb_Pg(Fl_Button*, void*) {
  fbrow->value(  fbrow->value( ) + 10 );
}

static void cb_Filter(Fl_Button*, void*) {
  loadpanel_filter( user_arg1 );
}

static void cb_Mark(Fl_Button*, void*) {
  loadpanel_mark( user_arg1 );
}

Fl_Output *statusbar=(Fl_Output *)0;

Fl_Double_Window* make_window() {
  { win1 = new Fl_Double_Window(875, 665, "FLVIEWFILTER");
    { Fl_Button* o = new Fl_Button(25, 605, 105, 25, "Re&load");
      o->labelfont(1);
      o->callback((Fl_Callback*)cb_Re);
    } // Fl_Button* o
    { fbrow = new Fl_Browser(5, 40, 865, 530);
      fbrow->labelsize( flfontsize  );
      fbrow->textsize( flfontsize  );
      fbrow->type(FL_HOLD_BROWSER);
    } // Fl_Browser* fbrow
    { Fl_Button* o = new Fl_Button(720, 635, 130, 25, "&Quit");
      o->labelfont(1);
      o->callback((Fl_Callback*)cb_Quit);
    } // Fl_Button* o
    { Fl_Button* o = new Fl_Button(775, 605, 75, 25, "Font (&+)");
      o->labelfont(1);
      o->callback((Fl_Callback*)cb_Font);
    } // Fl_Button* o
    { Fl_Button* o = new Fl_Button(695, 605, 75, 25, "Font (&-)");
      o->labelfont(1);
      o->callback((Fl_Callback*)cb_Font1);
    } // Fl_Button* o
    { Fl_Box* o = new Fl_Box(5, 5, 865, 30, "File Viewer Filter");
      o->box(FL_ENGRAVED_BOX);
      o->labeltype(FL_ENGRAVED_LABEL);
    } // Fl_Box* o
    { inputbox_filter_str = new Fl_Input(135, 575, 430, 25);
    } // Fl_Input* inputbox_filter_str
    { Fl_Button* o = new Fl_Button(695, 575, 75, 25, "Page &Up");
      o->callback((Fl_Callback*)cb_Page);
    } // Fl_Button* o
    { Fl_Button* o = new Fl_Button(775, 575, 75, 25, "Pg.Dw&n");
      o->callback((Fl_Callback*)cb_Pg);
    } // Fl_Button* o
    { Fl_Button* o = new Fl_Button(570, 575, 105, 25, "&Filter");
      o->labelfont(1);
      o->callback((Fl_Callback*)cb_Filter);
    } // Fl_Button* o
    { Fl_Button* o = new Fl_Button(25, 575, 105, 25, "&Mark");
      o->labelfont(1);
      o->callback((Fl_Callback*)cb_Mark);
    } // Fl_Button* o
    { statusbar = new Fl_Output(25, 635, 685, 25);
      statusbar->color(FL_BACKGROUND_COLOR);
    } // Fl_Output* statusbar
    win1->end();
    win1->resizable(win1);
  } // Fl_Double_Window* win1
  return win1;
}

int main( int argc, char *argv[]) {
  printf( " == FLVIEW == \n" );
  
    char mydirnow[2500];
    printf( "Current Directory: %s \n", getcwd( mydirnow, 2500 ) );
   
    char filein[PATH_MAX];
    char filesource[PATH_MAX];
    strncpy( filesource , "/etc/hostname" , PATH_MAX );
    strncpy( filein ,     filesource , PATH_MAX );
    if ( argc == 2)
    if ( strcmp( argv[1] , "" ) !=  0 ) 
    {
            strncpy( filein, argv[ 1 ], PATH_MAX );
            strncpy( filesource , argv[ 1 ], PATH_MAX );
    }
    strncpy( user_arg1, filein , PATH_MAX );
   
    make_window();
    
    loadpanel(   filein   );
    
    win1->show();
    Fl::run();
}
