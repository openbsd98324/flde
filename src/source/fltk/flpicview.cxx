

//////////////////////////////////////////
////////////////////////////////////////////////////////
// FLVIEW is a FLTK Simple App to view png  on NetBSD 
// g++ -lfltk -lfltk_images  flview.cxx  -o flview  
////////////////////////////////////////////////////////
//////////////////////////////////////////

//////////////////////////////////////////
//////////////////////////////////////////
#include <stdio.h>
#if defined(__linux__) //linux
#define MYOS 1
#elif defined(_WIN32)
#define MYOS 2
#elif defined(_WIN64)
#define MYOS 3
#elif defined(__unix__) 
#define MYOS 4  // freebsd
#define PATH_MAX 2500
#else
#define MYOS 0
#endif
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <dirent.h>
#include <sys/stat.h>
#include <unistd.h>  
//////////////////////////////////////////
#include <FL/Fl.H>
#include <FL/Fl_Window.H>
#include <FL/Fl_Button.H>
#include <FL/Fl_Box.H>
#include <FL/Fl_PNG_Image.H>
//////////////////////////////////////////


char fileread[PATH_MAX];
char fileimg[PATH_MAX];
// Global pointers for the GUI objects
Fl_Window *mywindow;
Fl_Button *lbutton;
Fl_Box *mypicturebox;


int fexist(const char *a_option)
{
  char dir1[PATH_MAX]; 
  char *dir2;
  DIR *dip;
  strncpy( dir1 , "",  PATH_MAX  );
  strncpy( dir1 , a_option,  PATH_MAX  );

  struct stat st_buf; 
  int status; 
  int fileordir = 0 ; 

  status = stat ( dir1 , &st_buf);
  if (status != 0) {
    fileordir = 0;
  }
  FILE *fp2check = fopen( dir1  ,"r");
  if( fp2check ) {
  fileordir = 1; 
  fclose(fp2check);
  } 

  if (S_ISDIR (st_buf.st_mode)) {
    fileordir = 2; 
  }
  return fileordir;
/////////////////////////////
}


/////////////////////////////
// FLTK starts...
/////////////////////////////
int modefileread = 1;
void cb_Update(void*) 
{
    printf( "Time\n" );
    strncpy( fileimg, fileread , PATH_MAX );
    /*
    //strncpy( fileimg, "car.png", PATH_MAX );
    //printf( "Fileread: %s\n\n", fileread );
    //if ( modefileread == 1 )
    //if ( fexist( fileread ) == 1 )
    {
               char getlinestr[PATH_MAX]; FILE *fptt;
               fptt = fopen( fileread , "rb+" );
               fgets( getlinestr , PATH_MAX, fptt ); 
               fclose( fptt );
               int i; 
               for(  i = 0; i < strlen( getlinestr ); i++ )
               {
                    if( getlinestr[i] == '\n')  getlinestr[i] = '\0';
               }
               if ( strcmp( getlinestr , "" ) != 0 )
               {
                    printf( "Read ini file (path): |%s|\n" , getlinestr );
                    strncpy( fileimg, getlinestr, PATH_MAX );
               }
    }
    */
    Fl_PNG_Image *limg;
    limg = new Fl_PNG_Image( fileimg );
    mypicturebox->image(limg);
    mypicturebox->redraw();
    mywindow->redraw();
    Fl::repeat_timeout(2.00, cb_Update);
}





//////////////////////////////////////////
//////////////////////////////////////////
int main( int argc, char *argv[])
{

    strncpy( fileread, "netbsd.png" , PATH_MAX );
    if ( argc == 2 )
    {
           strncpy( fileread, argv[ 1 ], PATH_MAX );
    }

    mywindow = new Fl_Window( 800, 600, "FLVIEW - FLTK Fast Image Viewer");
    mywindow->resizable(mywindow);

    mypicturebox = new Fl_Box(10, 10, 700, 500);
    strncpy( fileimg, fileread , PATH_MAX );
    Fl_PNG_Image *limg;
    limg = new Fl_PNG_Image( fileimg );
    mypicturebox->image(limg);
    mypicturebox->redraw();
    mywindow->redraw();

    mywindow->end();
    mywindow->show();
    //Fl::add_timeout(2.0, cb_Update);
    return(Fl::run());
}




