
//////////////////////////////////////////
#include <stdio.h>
#if defined(__linux__) //linux
#define MYOS 1
#elif defined(_WIN32)
#define MYOS 2
#elif defined(_WIN64)
#define MYOS 3
#elif defined(__unix__) 
#define MYOS 4  // freebsd
#define PATH_MAX 2500
#else
#define MYOS 0
#endif

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <limits.h>
#include <math.h>
#include <time.h>


#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <limits.h>

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <limits.h>
#include <Fl/Fl.H>
#include <Fl/fl_draw.H>
#include <Fl/Fl_Box.H>
#include <Fl/Fl_Window.H>
#include <Fl/Fl_Widget.H>
#include <Fl/Fl_Input.H>
#include <Fl/Fl_Output.H>
#include <FL/Fl_Button.H>




#include "tinyexpr.h"
char plotfunction[PATH_MAX];
char myfunction[PATH_MAX];
Fl_Input *input1;
Fl_Input *input2;



// eine unterklasse von Fl_Box zum Zeichnen herrichten
// sollte natürlich in eine eigene cpp mit eigenem header...
class DrawBox : public Fl_Box
{
public:
  int frame = 0;
  char charo[PATH_MAX];

  void draw()
  {
      fl_color( FL_WHITE);
      fl_rectf( 0, 22, w(), h() );

      // koordinatensystem zeichnen
      fl_color(  FL_BLUE );
      fl_xyline(     x()+10 , y()+h()-30 -350 , 350+ x()+w()-30 ) ;  // x-achse
      fl_draw(  "x",  x()+w()-20, -350 + y()+h()-27) ;

      fl_yxline( 350 + x()+30 , y()+h()-10, y()+40 ) ; // y-achse
      fl_draw(  "y", 350 + x()+28, y()+30) ;

      snprintf( charo, 250 , "%d %d", (int)time(NULL), frame);
      fl_draw(  charo , 40, 40 );

      int x = 20 ;  int i ; 
      char cmdline[PATH_MAX];
      char fetchlinetmp[PATH_MAX];
      char fetchline[PATH_MAX];

      strncpy( fetchlinetmp, " sin( X ) " , PATH_MAX );
      strncpy( fetchline, "" , PATH_MAX );
      strncpy( myfunction, input2->value() , PATH_MAX );
      fl_draw( myfunction , 40, 60 );

      fl_color(  FL_BLACK );
      char str[PATH_MAX];
      char charo[PATH_MAX];
      fl_color( FL_RED );
      double px ;  double py;  int fi;
      double popx ;  double popy; 
      strncpy( str, myfunction, PATH_MAX );
      char ptr[ 5* strlen( str )+1];
      for( px= -350; px<= 700 ; px+=1 )
      {
             int i,j=0;
             for(i=0; str[i]!='\0'; i++)
             {
               if ( str[i] == 'X' ) 
       	       {
                   ptr[j++] =  ' '; 
                   snprintf( charo, 250 , "%g", px );
                   for(fi=0; charo[fi]!='\0'; fi++)
                     ptr[j++] = charo[fi] ;
                   ptr[j++] =  ' '; 
       	       }
               else
       	       {
                 ptr[j++]=str[i];
       	       }
             } 
             ptr[j]='\0';

          popx = te_interp( ptr, 0 ) ; 
          fl_point( 350 + 30 +  px , 700 - 350  + 28 -  popx );
      }

   }


    static void Timer_CB(void *userdata) 
    {
        DrawBox *pb = (DrawBox*)userdata;
        pb->redraw();
        Fl::repeat_timeout(2.0, Timer_CB, userdata);
    }

     DrawBox(int x, int y, int w, int h, const char*l=0) : Fl_Box(x, y, w, h, l)
     {
        Fl::add_timeout( 2.0 , Timer_CB, (void*)this);     
     }
};





void drawit( Fl_Widget* obj, void*)
{
   strncpy( myfunction, input2->value() , PATH_MAX );
   //const char* temp;  
   // (  (Fl_Output*)(button->parent()->child(3))  )->value(temp);
   //input2->value(temp); 
   //strncpy( myfunction, temp , PATH_MAX );
}




///////////////////////////////////////////////////////////
///////////////////////////////////////////////////////////
///////////////////////////////////////////////////////////
int main( int argc, char *argv[])
{
    strncpy( plotfunction, " 100 ", PATH_MAX );
    strncpy( myfunction, "  4 * X ", PATH_MAX );
    ////////////////////////////////////////////////////////
    if ( argc == 2)
    if ( ( strcmp( argv[1] , "-time" ) ==  0 ) || ( strcmp( argv[1] , "--time" ) ==  0 ) )
    {
       printf("%d\n", (int)time(NULL));
       return 0;
    }


   Fl_Window win( 200, 200, 750, 750, "FLPLOT (FLTK) NetBSD(c)");
   DrawBox drawBox( 0, 0 , win.w(), win.h()) ;
   win.resizable(drawBox);

//   input1 = new Fl_Input( 2 ,  2  , 50 , 15 , "");
//   input1->value(" 200 * X ");
//   input1->color(FL_RED);

//   //Create a button - x , y , width, height, label
//   Fl_Button *button3 = new Fl_Button( 60 ,   2  , 50 , 15, "Plot!" );
//   button3->color(FL_BLUE);
//   button3->callback( (Fl_Callback*) drawit );

   input2 = new Fl_Input(   200 , 2  ,  100 , 15 , "F(X)=");
   input2->value(" 4 * X ");

   win.show();
   return Fl::run();


   return 0;
}




