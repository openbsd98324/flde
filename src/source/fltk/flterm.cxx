

#include <string.h>
#include <stdio.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <unistd.h>
#include <termios.h>
#include <signal.h>
#include <sys/ioctl.h>
#include <time.h>
#include <dirent.h>
#include <ctype.h>
#include <sys/stat.h>
#include <dirent.h>
#include <sys/types.h>
#include <unistd.h>  

////////////////////
#include <stdio.h>
#include <string.h>
#include <stdlib.h>
#include <dirent.h>
#include <sys/stat.h>
#include <dirent.h>
#include <unistd.h>  
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <dirent.h>
#include <time.h>
#if defined(__linux__) //linux
#define MYOS 1
#elif defined(_WIN32)
#define MYOS 2
#elif defined(_WIN64)
#define MYOS 3
#elif defined(__unix__) 
#define MYOS 4  // freebsd
#define PATH_MAX 2500
#else
#define MYOS 0
#endif

#include <FL/Fl.H>
#include <FL/Fl_Double_Window.H>
#include <FL/Fl_Text_Editor.H>


#include <math.h>
#include <unistd.h>  
#include <FL/Fl.H>
#include <FL/Fl_Window.H>
#include <FL/Fl_Double_Window.H>
#include <FL/fl_draw.H>
#include <FL/Fl_Box.H>
/// new
#include <FL/Fl_Input.H>
#include <FL/Fl_Output.H>
#include <FL/Fl_Button.H>
///#include <FL/fl_ask.H>


#ifdef _WIN32
#define popen  _popen
#define pclose _pclose
#endif
//
// Author of FLTK many thingsk: Erco 
//

char lastcmd[2500];









////////////////// Win Context Menu 
char  wincontextmenustr[2500];
Fl_Input  *wincontextmenuwin_in;
Fl_Output *wincontextmenuwin_out;
Fl_Double_Window* wincontextmenuwin;
void wincontextmenu_win_but1( Fl_Widget* obj, void*)
{
   strncpy( wincontextmenustr  , wincontextmenuwin_in->value() ,  sizeof( wincontextmenustr ) );
   wincontextmenuwin_out->value( wincontextmenustr );
}
void wincontextmenu_win_hide()
{
   wincontextmenuwin->hide();
}
void wincontextmenu_win_show()
{
    wincontextmenuwin = new Fl_Double_Window( 310, 300, "Context"); //280
    Fl_Button *button2 = new Fl_Button( 70, 260, 180, 24 , "Cancel" );
    button2->callback( (Fl_Callback*) wincontextmenu_win_hide );
    wincontextmenuwin_in =   new Fl_Input( 70, 20,  180, 24,   "Info");
    wincontextmenuwin_out =  new Fl_Output( 70, 200, 180, 24, "=");
    Fl_Button *button1 = new Fl_Button( 70, 230, 180, 24 , "Store!" );
    wincontextmenuwin_out->color((Fl_Color)48);
    button1->callback( (Fl_Callback*) wincontextmenu_win_but1 );
    wincontextmenuwin->show();
    while( wincontextmenuwin->shown() ) Fl::wait();
}













class MyTerminal : public Fl_Text_Editor {
    Fl_Text_Buffer *buff;
    char cmd[1024];
public:
    MyTerminal(int X,int Y,int W,int H,const char* L=0) : Fl_Text_Editor(X,Y,W,H,L) {
        buff = new Fl_Text_Buffer();
        buffer(buff);
        textfont(FL_COURIER);
        textsize(12);
        cmd[0] = 0;
    }
    // Append to buffer, keep cursor at end
    void append(const char*s) {
        buff->append(s);
        // Go to end of line
        insert_position(buffer()->length());
        scroll(count_lines(0, buffer()->length(), 1), 0);
    }
    // Run the specified command in the shell, append output to terminal
    void RunCommand(const char *command) {
        append("\n");
        fprintf(stderr, "EXECUTING: '%s'\n", command);
        FILE *fp = popen(command, "r");
        if ( fp == 0 ) {
            append("Failed to execute: '");
            append(command);
            append("'\n");
        } else {
            char s[1024];
            while ( fgets(s, sizeof(s)-1, fp) ) {
                append(s);
            }
            pclose(fp);
        }
    }


    // Handle events in the Fl_Text_Editor
    int handle(int e) {
        switch (e) {
            case FL_KEYUP: 
	    {
                int key = Fl::event_key();
                if ( key == FL_Enter ) return(1);              // hide Enter from editor
                if ( key == FL_BackSpace && cmd[0] == 0 ) return(0);
                break;
            }


            case FL_KEYDOWN: 
	    {
                int key = Fl::event_key();
                // Enter key? Execute the command, clear command buffer
                if ( key == 65472 ) //f3
		{
                      strncpy( cmd  , lastcmd ,    sizeof( cmd ) );
                      append("\nEnter a shell command: ");
                      append( cmd );
	        }
		else 
                if ( key == 65473 ) //f4
		{
                      strncpy( cmd  , " ls ",  sizeof( cmd ) );
                      strcat( cmd , " 2>&1");                // stderr + stdout
                      RunCommand( cmd );
                      cmd[0] = 0;
                      strncpy( cmd  , "" ,    sizeof( cmd ) );
                      append("\nEnter a shell command: ");
		}
                else if ( key == 65479 )  
		{
		    exit( 0 );
		}
                else if ( key == 65477 )  
		{
		    system( " screen -d -m flterm " );
		}
                else if ( key == 65478 )  // f9
		{
		    wincontextmenu_win_show();
		}
                else if ( key == FL_Enter ) 
		{

                    if ( strcmp( cmd , "cd .." ) == 0 ) 
          	    { 
                            printf( "\n" );
                            chdir( ".." );
                            printf( "\n" );
          	    } 

                    else if ( strcmp( cmd , "quit" ) == 0 ) 
          	    { 
                            printf( "\n" );
                            exit( 0 ); 
                            printf( "\n" );
          	    } 
                    else if ( strcmp( cmd , "exit" ) == 0 ) 
          	    { 
                            printf( "\n" );
                            exit( 0 ); 
                            printf( "\n" );
          	    } 
                    else if ( strcmp( cmd , "cd" ) == 0 ) 
          	    { 
                            printf( "\n" );
                            chdir( getenv( "HOME" ) );
                            printf( "\n" );
          	    } 
                    // Execute your commands here
		    else 
		    {
                      strncpy( lastcmd  , "",    sizeof( lastcmd ) );
                      strncpy( lastcmd  , cmd ,  sizeof( lastcmd ) );
                      strcat( cmd, " 2>&1");                // stderr + stdout
                      RunCommand( cmd );
                      //cmd[0] = 0;
		    }
                    cmd[0] = 0;
                    append("\nEnter a shell command: ");
                    return(1);                          // hide 'Enter' from text widget
                }
                if ( key == FL_BackSpace ) {
                    // Remove a character from end of command buffer
                    if ( cmd[0] ) {
                        cmd[strlen(cmd)-1] = 0;
                        break;
                    } else {
                        return(0);
                    }
                } else {
                    // Append text to our 'command' buffer
                    strncat(cmd, Fl::event_text(), sizeof(cmd)-1);
                    cmd[sizeof(cmd)-1] = 0;
                }
                break;
            }
        }
        return(Fl_Text_Editor::handle(e));
    }
};













////////////////////////////////////////
int main( int argc, char *argv[])
{
        strncpy( lastcmd  , "",  sizeof( lastcmd ) );
        int i;
	char envhome[PATH_MAX];
	strncpy( envhome, getenv( "HOME" ), PATH_MAX );
	////////////////////////////////////////////////////////
	if ( argc == 2)
	if ( strcmp( argv[1] , "" ) !=  0 )
	{
   	    chdir( argv[ 1 ] );
	}

    Fl_Double_Window win(620,520,"Terminal");
    MyTerminal edit(10,10,win.w()-20,win.h()-20);
    edit.append("FLTK TERMINAL\n");
    edit.append("F1: Help/Info.\n");
    edit.append("F3: retype\n");
    edit.append("F4: ls\n");
    edit.append("F8: New flterm Console.\n");
    edit.append("F9: Context menu\n");
    edit.append("Enter a shell command: ");
    win.resizable(win);
    win.show();
    return(Fl::run());
}



