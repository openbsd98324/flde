// generated by Fast Light User Interface Designer (fluid) version 1.0304

#include "fxplot.h"
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include "tinymath.h"

Fl_Double_Window *win1=(Fl_Double_Window *)0;

Fl_Input *form1_tab1_grapher_plot_filesource=(Fl_Input *)0;

static void cb_Edit(Fl_Button*, void*) {
  // form1_tab1_grapher_plot_filesource

if ( fexist( form1_tab1_grapher_plot_filesource->value() ) == 1 )
  {
        // form1_tab1_diffusion_filesource->value() 
        char cmdi[PATH_MAX];
        strncpy( cmdi , " screen -d -m flnotepad  "  , PATH_MAX );
        strncat( cmdi , " " , PATH_MAX - strlen( cmdi ) -1 );
        strncat( cmdi , " \""  , PATH_MAX - strlen( cmdi ) -1 );
        strncat( cmdi , form1_tab1_grapher_plot_filesource->value() , PATH_MAX - strlen( cmdi ) -1 );
        strncat( cmdi , "\" "  , PATH_MAX - strlen( cmdi ) -1 );
        strncat( cmdi , " " , PATH_MAX - strlen( cmdi ) -1 );
        system( cmdi ); 
   }
   
 //   plot [-10:10] [-5:5]  x**2, x**3, x , 0;
}

static void cb_View(Fl_Button*, void*) {
  if ( fexist( form1_tab1_grapher_plot_filesource->value() ) == 1 )
  {
        // form1_tab1_diffusion_filesource->value() 
        char cmdi[PATH_MAX];
        strncpy( cmdi , "  screen -d -m  gnuplot  -p    "  , PATH_MAX );
        strncat( cmdi , " " , PATH_MAX - strlen( cmdi ) -1 );
        strncat( cmdi , " \""  , PATH_MAX - strlen( cmdi ) -1 );
        strncat( cmdi , form1_tab1_grapher_plot_filesource->value() , PATH_MAX - strlen( cmdi ) -1 );
        strncat( cmdi , "\" "  , PATH_MAX - strlen( cmdi ) -1 );
        strncat( cmdi , " " , PATH_MAX - strlen( cmdi ) -1 );
        system( cmdi ); 
   };
}

static void cb_Close(Fl_Button*, void*) {
  // form1_tab1_diffusion_filesource->value() 
        char cmdi[PATH_MAX];
     
        system( " pkill gnuplot " );
}

static void cb_Plot(Fl_Button*, void*) {
  FILE *fp6;

fp6 = fopen( form1_tab1_grapher_plot_filesource->value() , "wb+");

  fputs( "\n" , fp6);
  //fputs( "   plot [-10:10] [-5:5]  x**2, x**3, x , 0 ;\n" , fp6);
  //  fputs( "   plot [-10:10] [-5:5]  " , fp6);
  
  if ( fxgrapher_mode_x11_driver_on->value( ) == 1 ) 
  {
        fputs( "   set termin x11   " , fp6);
        fputs( "  \n   " , fp6);
        // x11 wxt and qt 
  }
  
  
  fputs( "   plot   " , fp6);
  fputs( form1_tab1_grapher_xrange->value(), fp6 );
  fputs( " " , fp6);
  fputs( form1_tab1_grapher_yrange->value(), fp6 );
  fputs( " " , fp6);


  if ( fx_check_active_fx->value(  ) == 1 )
  {
     fputs( form1_tab1_grapher_function_fx->value( ) , fp6);
  }
  
  if ( fx_check_active_gx->value(  ) == 1 )
  {
     if ( fx_check_active_fx->value(  ) == 1 )  fputs( " , " , fp6);
     fputs( form1_tab1_grapher_function_gx->value( ) , fp6);
  }

  if ( fx_check_active_hx->value(  ) == 1 )
  {
      if ( fx_check_active_gx->value(  ) == 1 )  fputs( " , " , fp6);
      fputs( form1_tab1_grapher_function_hx->value( ) , fp6);
  }
  fputs( " ;\n " , fp6);
  
  fputs( "\n" , fp6);
  fputs( "   pause -1 ;\n" , fp6);
  fputs( "\n" , fp6);

fclose( fp6 );
 

if ( fexist( form1_tab1_grapher_plot_filesource->value() ) == 1 )
  {
        // form1_tab1_diffusion_filesource->value() 
        char cmdi[PATH_MAX];
        strncpy( cmdi , "  screen -d -m  gnuplot  -p    "  , PATH_MAX );
        strncat( cmdi , " " , PATH_MAX - strlen( cmdi ) -1 );
        strncat( cmdi , " \""  , PATH_MAX - strlen( cmdi ) -1 );
        strncat( cmdi , form1_tab1_grapher_plot_filesource->value() , PATH_MAX - strlen( cmdi ) -1 );
        strncat( cmdi , "\" "  , PATH_MAX - strlen( cmdi ) -1 );
        strncat( cmdi , " " , PATH_MAX - strlen( cmdi ) -1 );
        system( cmdi ); 
   };
}

Fl_Input *form1_tab1_grapher_function_fx=(Fl_Input *)0;

Fl_Input *form1_tab1_grapher_function_gx=(Fl_Input *)0;

Fl_Input *form1_tab1_grapher_function_hx=(Fl_Input *)0;

Fl_Check_Button *fx_check_active_fx=(Fl_Check_Button *)0;

Fl_Check_Button *fx_check_active_gx=(Fl_Check_Button *)0;

Fl_Check_Button *fx_check_active_hx=(Fl_Check_Button *)0;

static void cb_Reset(Fl_Button*, void*) {
  FILE *fp6;

fp6 = fopen( form1_tab1_grapher_plot_filesource->value() , "wb+");

  fputs( "\n" , fp6);
  fputs( "   plot [-10:10] [-5:5]  x**2, x**3, x , 0 ;\n" , fp6);
  fputs( "\n" , fp6);

  fputs( "\n" , fp6);
  fputs( "   pause -1 ;\n" , fp6);
  fputs( "\n" , fp6);

fclose( fp6 );

form1_tab1_grapher_function_fx->value( " x**2" );
form1_tab1_grapher_function_gx->value( " x**3" );
form1_tab1_grapher_function_hx->value( " 0" );
}

Fl_Input *form1_tab1_grapher_xrange=(Fl_Input *)0;

Fl_Input *form1_tab1_grapher_yrange=(Fl_Input *)0;

static void cb_CA(Fl_Button*, void*) {
  form1_tab1_grapher_xrange->value( " " );

form1_tab1_grapher_yrange->value( " " );
}

static void cb_Default(Fl_Button*, void*) {
  form1_tab1_grapher_xrange->value( "[-10:10]" );

form1_tab1_grapher_yrange->value( "[-5:5]" );
}

static void cb_10(Fl_Button*, void*) {
  form1_tab1_grapher_xrange->value( "[-10:10]" );

form1_tab1_grapher_yrange->value( "[-10:10]" );
}

static void cb_X(Fl_Button*, void*) {
  form1_tab1_grapher_xrange->value( "[0:1000]" );
form1_tab1_grapher_yrange->value( " " );
}

static void cb_CA1(Fl_Button*, void*) {
  form1_tab1_grapher_xrange->value( " " );
}

static void cb_CA2(Fl_Button*, void*) {
  form1_tab1_grapher_yrange->value( " " );
}

Fl_Check_Button *fxgrapher_mode_x11_driver_on=(Fl_Check_Button *)0;

static void cb_Close1(Fl_Button*, void*) {
  exit( 0 );
}

Fl_Output *win1_statusbar_content=(Fl_Output *)0;

Fl_Double_Window* make_window() {
  { win1 = new Fl_Double_Window(835, 750, "FxPlot");
    win1->box(FL_DOWN_BOX);
    { Fl_Box* o = new Fl_Box(20, 15, 800, 40, "FxPlot");
      o->box(FL_ENGRAVED_BOX);
      o->labelfont(1);
    } // Fl_Box* o
    { Fl_Tabs* o = new Fl_Tabs(20, 80, 800, 630, "User Parameter");
      { Fl_Group* o = new Fl_Group(40, 105, 780, 605, "Graph");
        { Fl_Group* o = new Fl_Group(450, 495, 340, 180, "Plot file");
          o->box(FL_DOWN_BOX);
          o->labeltype(FL_ENGRAVED_LABEL);
          { form1_tab1_grapher_plot_filesource = new Fl_Input(540, 510, 225, 25, "Plot file:");
            form1_tab1_grapher_plot_filesource->value( "fxgrapher.plt" );
          } // Fl_Input* form1_tab1_grapher_plot_filesource
          { Fl_Button* o = new Fl_Button(480, 555, 120, 30, "Edit Plot");
            o->callback((Fl_Callback*)cb_Edit);
          } // Fl_Button* o
          { Fl_Button* o = new Fl_Button(480, 590, 120, 30, "View Plot");
            o->callback((Fl_Callback*)cb_View);
          } // Fl_Button* o
          { Fl_Button* o = new Fl_Button(480, 625, 120, 30, "Close all plots");
            o->callback((Fl_Callback*)cb_Close);
          } // Fl_Button* o
          o->end();
        } // Fl_Group* o
        { Fl_Group* o = new Fl_Group(45, 155, 750, 280, "Graphical Calculator, y=f(x)");
          o->box(FL_DOWN_BOX);
          o->labeltype(FL_ENGRAVED_LABEL);
          { Fl_Button* o = new Fl_Button(55, 380, 595, 45, "&Plot Graphs !");
            o->labelfont(1);
            o->callback((Fl_Callback*)cb_Plot);
          } // Fl_Button* o
          { form1_tab1_grapher_function_fx = new Fl_Input(85, 175, 660, 25, "f(x)");
            form1_tab1_grapher_function_fx->value( "x**2" );
          } // Fl_Input* form1_tab1_grapher_function_fx
          { form1_tab1_grapher_function_gx = new Fl_Input(85, 215, 660, 25, "g(x)");
            form1_tab1_grapher_function_gx->value( "x**3" );
          } // Fl_Input* form1_tab1_grapher_function_gx
          { form1_tab1_grapher_function_hx = new Fl_Input(85, 255, 660, 25, "h(x)");
            form1_tab1_grapher_function_hx->value( "0" );
          } // Fl_Input* form1_tab1_grapher_function_hx
          { fx_check_active_fx = new Fl_Check_Button(755, 175, 25, 25);
            fx_check_active_fx->down_box(FL_DOWN_BOX);
          } // Fl_Check_Button* fx_check_active_fx
          { fx_check_active_gx = new Fl_Check_Button(755, 215, 25, 25);
            fx_check_active_gx->down_box(FL_DOWN_BOX);
          } // Fl_Check_Button* fx_check_active_gx
          { fx_check_active_hx = new Fl_Check_Button(755, 255, 25, 25);
            fx_check_active_hx->down_box(FL_DOWN_BOX);
          } // Fl_Check_Button* fx_check_active_hx
          { Fl_Button* o = new Fl_Button(680, 380, 105, 45, "Reset funcs");
            o->callback((Fl_Callback*)cb_Reset);
          } // Fl_Button* o
          { new Fl_Box(115, 350, 25, 25, "Gnu math formalism");
          } // Fl_Box* o
          o->end();
        } // Fl_Group* o
        { Fl_Group* o = new Fl_Group(45, 495, 395, 180, "Range");
          o->box(FL_DOWN_BOX);
          o->labeltype(FL_ENGRAVED_LABEL);
          { form1_tab1_grapher_xrange = new Fl_Input(135, 510, 225, 25, "X range");
            form1_tab1_grapher_xrange->value( "[-10:10]" );
          } // Fl_Input* form1_tab1_grapher_xrange
          { form1_tab1_grapher_yrange = new Fl_Input(135, 545, 225, 25, "Y range");
            form1_tab1_grapher_yrange->value( "[-5:5]" );
          } // Fl_Input* form1_tab1_grapher_yrange
          { Fl_Button* o = new Fl_Button(55, 595, 90, 25, "CA");
            o->callback((Fl_Callback*)cb_CA);
          } // Fl_Button* o
          { Fl_Button* o = new Fl_Button(150, 595, 90, 25, "Default");
            o->callback((Fl_Callback*)cb_Default);
          } // Fl_Button* o
          { Fl_Button* o = new Fl_Button(245, 595, 90, 25, "[-10:10]");
            o->callback((Fl_Callback*)cb_10);
          } // Fl_Button* o
          { Fl_Button* o = new Fl_Button(340, 595, 90, 25, "X [0:1000]");
            o->callback((Fl_Callback*)cb_X);
          } // Fl_Button* o
          { Fl_Button* o = new Fl_Button(360, 510, 30, 25, "CA");
            o->callback((Fl_Callback*)cb_CA1);
          } // Fl_Button* o
          { Fl_Button* o = new Fl_Button(360, 545, 30, 25, "CA");
            o->callback((Fl_Callback*)cb_CA2);
          } // Fl_Button* o
          { fxgrapher_mode_x11_driver_on = new Fl_Check_Button(55, 635, 25, 25, "Plot driver with X11 Library");
            fxgrapher_mode_x11_driver_on->down_box(FL_DOWN_BOX);
            fxgrapher_mode_x11_driver_on->value( 0 );
          } // Fl_Check_Button* fxgrapher_mode_x11_driver_on
          o->end();
        } // Fl_Group* o
        o->end();
      } // Fl_Group* o
      o->end();
    } // Fl_Tabs* o
    { Fl_Button* o = new Fl_Button(705, 715, 115, 25, "&Close");
      o->labelfont(1);
      o->callback((Fl_Callback*)cb_Close1);
    } // Fl_Button* o
    { win1_statusbar_content = new Fl_Output(20, 715, 680, 25);
      win1_statusbar_content->color(FL_BACKGROUND_COLOR);
      win1_statusbar_content->value( "" );
    } // Fl_Output* win1_statusbar_content
    win1->end();
    win1->resizable(win1);
  } // Fl_Double_Window* win1
  return win1;
}

int main( int argc, char *argv[]) {
  printf( " == FLTK == \n" );
  
    
    if ( argc == 2 )
    if ( strcmp( argv[1] , "" ) !=  0 )
    {
           printf( "chdir %s\n" , argv[ 1 ] ); 
           chdir( argv[ 1 ] );
    }
    
  
          make_window();
    
   	//browser1->type(FL_HOLD_BROWSER);
  	//browser1->add("============");  
  	//browser1->add(" CALCULATOR ");    
  	//browser1->add("============");
  	
  	fx_check_active_fx->value( 1 );
  	fx_check_active_gx->value( 1 );
  	fx_check_active_hx->value( 1 );
  	
     
    win1->show();
  
    Fl::run();
}
