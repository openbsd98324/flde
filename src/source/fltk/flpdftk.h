// generated by Fast Light User Interface Designer (fluid) version 1.0304

#ifndef flpdftk_h
#define flpdftk_h
#include <FL/Fl.H>
int fexist( const char *a_option );
#include <FL/Fl_Double_Window.H>
extern Fl_Double_Window *win1;
#include <FL/Fl_Button.H>
#include <FL/Fl_Box.H>
#include <FL/Fl_Tabs.H>
#include <FL/Fl_Group.H>
#include <FL/Fl_Input.H>
extern Fl_Input *var_pdftk_cut_pagenumber_str;
#include <FL/Fl_Pack.H>
#include <FL/Fl_Check_Button.H>
extern Fl_Check_Button *var_button_xterm_logdebug;
#include <FL/Fl_Output.H>
extern Fl_Output *var_inputbox_gs_example1;
extern Fl_Output *statusline;
extern Fl_Input *var_filesource_str;
int make_window();
int main( int argc, char *argv[]);
#endif
