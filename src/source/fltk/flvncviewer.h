// generated by Fast Light User Interface Designer (fluid) version 1.0304

#ifndef flvncviewer_h
#define flvncviewer_h
#include <FL/Fl.H>
int fexist( const char *a_option );
#include <FL/Fl_Double_Window.H>
extern Fl_Double_Window *window_run_command;
#include <FL/Fl_Box.H>
#include <FL/Fl_Input.H>
extern Fl_Input *userinput_main_command;
#include <FL/Fl_Button.H>
#include <FL/Fl_Check_Button.H>
extern Fl_Check_Button *var_mode_vnc_option_bgr233;
Fl_Double_Window* make_window();
int main( int argc, char *argv[]);
#endif
