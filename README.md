# FLDE - Fast Light Desktop


FLDE: desktop for linux, ideally suited for Raspberry PI and arm boards
It is stable on Linux, BSD, and other X. It offers a lightweight desktop and it gives modular environment for your desktop, based on FLTK. Each application can be compiled on its own using C/C++ compiler. It uses the FLTK library.

Screenshot:
![](media/freebsd-13-desktop-glxgears-fast-pc-1644940981.png)






## FLDE Education for classrooms and schools 

fldict with wiktionary using wordnet, wikitionary, fltranslate,... . 

![](media/flde-dict.png)


![](media/flde-flsciences-fldict-v1.png)

![](medias/1694429490-screenshot.png)



![](media/flde-edu-language-fltranslate-v1.png)






## FLDE Sciences

![](media/flde-sciences-pic01.png)


![](media/flde-mathomatic-v2.png)

 
![](media/flde-sciences-avogadro-v2.png)

![](media/flde-sciences-avogadro-acid1-v3.png)




## Maths 

mathomatic, fxgrapher, fxplot, flgnuplot,... are a complete suite for maths. 

![](media/1631128413-screenshot.png)

(...)


## FxPlot 
An easy frontend for Gnuplot. 
Calculations, maths,...  with Gnuplot.

![](media/1655184131-screenshot.png)



More complex graphs for higher graduate level: 

![](media/fxgrapher.png)



## Kicker 

The main bar was called "kicker", alike the original kicker bar of GNU KDE 1.x.



![](media/1658039287-screenshot.png)




## FLM 

FLM is the default file manager.


![](media/flm-1.png)






## FLWORKER 

flworker is the default two-panel file manager (fltk).

![](media/flworker.png)



## FLSETTING

Settings and disk management. 

![](media/1658037403-screenshot.png)





## FLTK Window Manager 

The WM is flevilwm. It is a custom fork of evilwm. 
The WM used follow mouse focus, i.e. like the "classic Unix desktop".



Keys Combination: 

Flag+e : File Manager (flm)

Flag+r : Window Command (flcommand)

Flag+t : Kicker bar (kicker)

Flag+h,j,k,l : Move the current window

Flag+w: Close the current window

Flag+mouse left: Move the current window

Flag+mouse right: Resize the current window



kicker bar: 
![](media/1630751392-fltk-flde.png)




## Utilities

Contact, Data, Jotter, Clock (flclock), Countdown, World Map, (...), Map/Coordinates of World Cities,...



![](media/1658038381-screenshot.png)

![](media/1658038414-screenshot.png)

![](media/1658038434-screenshot.png)

![](media/1658038457-screenshot.png)

![](media/1658050804-screenshot.png)





## FlMencoder 

Cut easily videos, using ffmpeg. 

![](media/flmencoder-1.png)

![](media/flmencoder-2.png)

![](media/flmencoder-3.png)




## Retro-Gaming

It uses the framebuffer of the Raspberry PI. 

![](media/1651422455-screenshot.png)






## Platforms

OpenBSD, FreeBSD, NetBSD, Linux, Solaris,...

It requires C++ to build FLDE from source.


### Running on all Linux distributions (OpenSuse, Slackware, Devuan,...)

- Ubuntu 22.10, x86_64:

![](media/1671354747-screenshot-linux-ubuntu-22.10-x86_64-notebook-1.png)



- Ubuntu Jammy 

3 Monitors, kmix.

![](medias/1701266289-screenshot-1.png)

![](medias/1701266752-screenshot-2.png)

![](medias/1701269796-screenshot-3.png)



- OpenSuse with Leap 15.3, on a notebook with KDE or XFCE:

c++ with fltk-devel allows to compile FLDE. 

![](media/opensuse/1637529193-screenshot.png)

![](media/1665159801-screenshot-opensuse-leap-15.3-flde.png)

![](media/1665205151-opensuse-flde-desktop-wireless-wicked.png)

![](media/1665205161-opensuse-flde-desktop-xfce-settings.png)


- Devuan

- Debian

(...)


### Running on *BSD (OpenBSD, FreeBSD, and NetBSD)

- FreeBSD: 

![](media/eeepc-desktop-fl-1-freebsd-13.png)

- NetBSD, OpenBSD.



### Raspberry Pi 

- Raspberry Pi 3 (rpi3b, rpi3b+):

![](media/1658037403-screenshot.png)



- Raspberry Pi 4, 4GB:

![](media/2022-12-18-092037_1920x1080_scrot.png)

Linux raspberrypi 5.15.61-v8+


## Pinebook-Pro

![](media/1679314614-screenshot.png)


### FLDE on ARM Pinebook Pro

![](medias/1692691508-screenshot.png)

Manjaro aarch64, fltk lib. 


### FLDE on Raspberry PI RPI3B, Raspbian / DEBIAN  

![](medias/linux-flde-fltk-desktop-lx-r1.1-1695227195.png) 


Shadow using the command "xcompmgr -c".




# CTWM On PineBook Pro 

Manjaro aarch64

![](medias/1699837734-screenshot.png) 



# XFCE4 + CTWM On PineBook Pro 

![](medias/1699839397-screenshot.png) 

![](medias/1699844355-screenshot.png)


The desktop is combined with FLTK/XFCE4 and installed using pacstrap. 

Pipewire is installed for the sound. left and right working. 



Optimized desktop, low end, fast and reliable. 

xcompmgr for shadow 

Theme win98 



### Build


Tested with current dependency:

````
pi@retropie:~ $ dpkg -l | grep fltk
ii  fltk1.3-doc                       1.3.4-9                             all          Fast Light Toolkit - documentation
ii  libfltk-cairo1.3:armhf            1.3.4-9                             armhf        Fast Light Toolkit - Cairo rendering layer support
ii  libfltk-forms1.3:armhf            1.3.4-9                             armhf        Fast Light Toolkit - Forms compatibility layer support
ii  libfltk-gl1.3:armhf               1.3.4-9                             armhf        Fast Light Toolkit - OpenGL rendering support
ii  libfltk-images1.3:armhf           1.3.4-9                             armhf        Fast Light Toolkit - image loading support
ii  libfltk1.3:armhf                  1.3.4-9                             armhf        Fast Light Toolkit - main shared library
ii  libfltk1.3-dev                    1.3.4-9                             armhf        Fast Light Toolkit - development files
````




