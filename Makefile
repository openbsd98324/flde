

all:
	  c++  src/source/fltk/flmessage.cxx  -lX11 -lfltk  -o  src/source/bin/flmessage   
	  c++  src/source/fltk/kicker.cxx  -lX11 -lfltk  -o  src/source/bin/kicker  
	  c++  src/source/fltk/flsystem.cxx  -lX11 -lfltk  -o  src/source/bin/flsystem   
	  c++  src/source/fltk/flm.cxx  -lX11 -lfltk     -o  src/source/bin/flm   
	  c++  src/source/fltk/flworker.cxx  -lX11 -lfltk     -o  src/source/bin/flworker     
	  c++  src/source/fltk/flclock.cxx  -lX11 -lfltk     -o  src/source/bin/flclock    
	  c++  src/source/fltk/fltimezone.cxx  -lX11 -lfltk     -o  src/source/bin/fltimezone     
	  c++  src/source/fltk/flstreamtuner.cxx  -lX11 -lfltk     -o  src/source/bin/flstreamtuner   




all-install:
	   zypper install make fltk-devel g++ clang gcc tmux screen 

help:
	echo RUN make help is this help
	echo RUN make all 
	echo RUN make install  
	echo RUN make debian for little files for debian installation of flde 

flworker:
	  c++  src/source/fltk/flworker.cxx  -lX11 -lfltk     -o  src/source/bin/flworker      
	  cc src/source/utils/ncrun.c -o src/source/bin/ncrun 

flsetting:
	  c++  src/source/fltk/flsetting.cxx  -lX11 -lfltk     -o  src/source/bin/flsetting       




flm:
	  c++  src/source/fltk/flm.cxx  -lX11 -lfltk     -o  src/source/bin/flm     
	  cc src/source/utils/ncrun.c -o src/source/bin/ncrun 
	  c++  src/source/fltk/flview.cxx  -lX11 -lfltk     -o  src/source/bin/flview     

fldf:
	  c++  src/source/fltk/fldf.cxx  -lX11 -lfltk     -o  src/source/bin/fldf    
	  c++  src/source/fltk/flview.cxx  -lX11 -lfltk     -o  src/source/bin/flview     

ncrun:
	  cc src/source/utils/ncrun.c -o src/source/bin/ncrun 


kicker:
	  c++  src/source/fltk/kicker.cxx  -lX11 -lfltk     -o  src/source/bin/kicker       
	  cc src/source/utils/ncrun.c -o src/source/bin/ncrun 

all-test-install:
	   sh install.sh

debian:
	  c++  src/source/fltk/kicker.cxx  -lX11 -lfltk  -o  src/source/bin/kicker  
	  c++  src/source/fltk/flsystem.cxx  -lX11 -lfltk  -o  src/source/bin/flsystem   
	  c++  src/source/fltk/flm.cxx  -lX11 -lfltk     -o  src/source/bin/flm   
	  c++  src/source/fltk/flworker.cxx  -lX11 -lfltk     -o  src/source/bin/flworker     
	  c++  src/source/fltk/flclock.cxx  -lX11 -lfltk     -o  src/source/bin/flclock    
	  c++  src/source/fltk/fltimezone.cxx  -lX11 -lfltk     -o  src/source/bin/fltimezone     
	  c++  src/source/fltk/flstreamtuner.cxx  -lX11 -lfltk     -o  src/source/bin/flstreamtuner   


flevilwm:
	  cc -DVWM src/source/evilwm/*.c  -lX11 -o  src/source/bin/evilwm 
	  cc -DVWM src/source/evilwm/*.c  -lX11 -o  /usr/local/bin/evilwm 
	  cc -DVWM src/source/evilwm/*.c  -lX11 -o  /usr/local/bin/flevilwm 
	  echo Compile nloop  
	  cc src/source/base/nloop.c -o src/source/bin/nloop 



evilwm:
	  cc -DVWM src/source/evilwm/*.c  -lX11 -o  src/source/bin/evilwm 
	  cc -DVWM src/source/evilwm/*.c  -lX11 -o  /usr/local/bin/evilwm 
	  cc -DVWM src/source/evilwm/*.c  -lX11 -o  /usr/local/bin/flevilwm 
	  echo Compile nloop  
	  cc src/source/base/nloop.c -o src/source/bin/nloop 


girmake:
	   cc -lm  src/source/base/girmake.c -o /usr/local/bin/girmake 


install:
	   mkdir /usr/local/ ; mkdir /usr/local/bin ; mv  src/source/bin/*  /usr/local/bin/ 


mcompile:
	  mconfig --compile-fltk  src/source/fltk/*.cxx  
	  echo Compile Completed  

ncompile:
	  nconfig --compile-fltk  src/source/fltk/*.cxx  
	  echo Compile Completed  

nloop:
	  echo Compile nloop  
	  cc src/source/base/nloop.c -o src/source/bin/nloop 


openevilwm:
	  echo Compile   
	  mkdir source ; mkdir source/bin ;  cc  -DVWM -lm   -I"/usr/X11R6/include/" -I"/usr/local/include" -I /usr/local/include/    -L"/usr/local/lib"  -L/usr/X11R6/lib -lX11 -I /usr/X11R6/include/   src/source/evilwm/*.c       -o   source/bin/evilwm


renunixtime:
	  echo compile ;  cc src/source/utils/renunixtime.c -o  src/source/bin/renunixtime


